# Copyright 2019-2020, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

option('drivers',
	type: 'array',
	choices: ['auto', 'dummy', 'hdk', 'hydra', 'ns', 'ohmd', 'psmv', 'psvr', 'rs', 'v4l2', 'vive', 'survive'],
	value: ['auto'],
	description: 'Set of drivers to build')

option('docs',
	type: 'feature',
	value: 'auto',
	description: 'Build the documentation')

option('doxygen-warn-undocumented',
	type: 'boolean',
	value: false,
	description: 'Configure doxygen to emit warnings for undocumented entities')

option('doxygen-extract-all',
	type: 'boolean',
	value: false,
	description: 'Extract all entities for documentation, not just documented ones')

option('gui',
	type: 'feature',
	value: 'auto',
	description: 'Enable GUI')

option('tracking',
	type: 'feature',
	value: 'auto',
	description: 'Enable tracking support')

option('install-active-runtime',
	type: 'boolean',
	value: true,
	description: 'Make Monado the default OpenXR runtime on install')

option('opengl',
	type: 'feature',
	value: 'auto',
	description: 'Enable OpenGL application support.')

option('opengles',
	type: 'feature',
	value: 'auto',
	description: 'Enable OpenGL|ES application support.')

option('egl',
	type: 'feature',
	value: 'auto',
	description: 'Enable EGL application support.')

option('xlib',
	type: 'feature',
	value: 'auto',
	description: 'Enable xlib application support. Also required for direct mode on X.')

option('xcb',
	type: 'feature',
	value: 'auto',
	description: 'Enable xcb support for direct mode on X.')

option('wayland',
	type: 'feature',
	value: 'auto',
	description: 'Enable support for Wayland rendering.')

option('service',
	type: 'boolean',
	value: true,
	description: 'Enable separate service module for OpenXR runtime'
)

option('layer_depth',
	type: 'boolean',
	value: true,
	description: 'Enable support for Depth Layers'
)

option('layer_cube',
	type: 'boolean',
	value: false,
  description: 'Enable support for Cube Layers'
)

option('layer_cylinder',
	type: 'boolean',
	value: true,
  description: 'Enable support for Cylinder Layers'
)

option('layer_equirect',
	type: 'boolean',
	value: false,
  description: 'Enable support for Equirect Layers'
)
