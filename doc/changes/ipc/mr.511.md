ipc: Add functionality to disable a device input via the `monado-ctl` utility,
this allows us to pass the conformance tests that requires the runtime to turn
off a device.
