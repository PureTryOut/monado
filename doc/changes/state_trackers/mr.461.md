OpenXR: Refactor the native compositor handling a bit, this creates the
compositor earlier then before. This allows us to get the viewport information
from it.
