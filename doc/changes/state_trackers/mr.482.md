OpenXR: Update to 1.0.11 and start returning the new `XR_ERROR_GRAPHICS_REQUIREMENTS_CALL_MISSING` code added in this release.
