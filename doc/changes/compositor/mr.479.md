compositor and clients: Use a generic typedef to represent the platform-specific graphics buffer, allowing use of `AHardwareBuffer` on recent Android.
