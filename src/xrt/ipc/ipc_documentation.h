// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  IPC documentation.
 * @author Jakob Bornecrantz <jakob@collabora.com>
 * @ingroup ipc
 */

#pragma once


/*!
 * @defgroup ipc Inter-Process Communication
 * @ingroup xrt
 *
 * @brief Inter-Process Communication layer for Monado.
 */

/*!
 * @defgroup ipc_client Client IPC
 * @ingroup ipc
 *
 * @brief Client side IPC code.
 */

/*!
 * @defgroup ipc_server Server IPC
 * @ingroup ipc
 *
 * @brief Server side IPC code.
 */

/*!
 * @dir xrt/ipc
 * @ingroup xrt
 *
 * @brief Inter-Process Communication layer
 */
